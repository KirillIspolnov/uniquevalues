package org.kirillbogatikov.unique_values;

import java.util.Arrays;
import java.util.Scanner;

public class Variant3 {
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.print("Кол-во элементов: ");
        int count = scanner.nextInt();
        
        String[] array = new String[count];
        
        int ucount = 0;
        String temp;
        for(int i = 0; i < count; i++) {
            temp = scanner.next();
            if(indexOf(array, temp) == -1) {
                array[ucount++] = temp;
            }
        }
        
        array = Arrays.copyOf(array, ucount);
        
        System.out.println(Arrays.toString(array));
    }

    public static int indexOf(String[] array, String d) {
        for(int i = 0; i < array.length; i++) {
            if(array[i] == null) break;
            if(array[i].equals(d))
                return i;
        }
        return -1;
    }
}
