package org.kirillbogatikov.unique_values;

import java.util.HashSet;
import java.util.Scanner;

public class Variant1 {
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.print("Кол-во элементов: ");
        int count = scanner.nextInt();
        
        HashSet<String> sequence = new HashSet<String>();
        
        for(int i = 0; i < count; i++) {
            //добавляем элемент в множество, если его там ещё нет
            sequence.add(scanner.next());
        }
        
        //выводим множество
        System.out.println(sequence);
    }

}
