package org.kirillbogatikov.unique_values;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Launcher {
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.print("Выберите метод решения задачи: 1 - множество, 2 - список, 3 - массив: ");
        int mode = scanner.nextInt();
        
        System.out.print("Выберите режим ввода данных: 1 - с клавиатуры, 2 - из файла: ");
        if(scanner.nextInt() == 2) {
            System.out.print("Введите путь к файлу с исходными данными: ");
            String path = readPath(); 
            try {
                System.setIn(new FileInputStream(path));
            } catch(IOException ioe) {
                ioe.printStackTrace();
            }
        }
        
        if(mode == 1) {
            Variant1.main(args);
        } else if(mode == 2) {
            Variant2.main(args);
        } else if(mode == 3) {
            Variant3.main(args);
        }
    }

    public static String readPath() {
        scanner.useDelimiter("\n");
        scanner.next();
        return scanner.next().trim();
    }
}
