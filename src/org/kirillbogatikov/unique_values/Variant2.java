package org.kirillbogatikov.unique_values;

import java.util.ArrayList;
import java.util.Scanner;

public class Variant2 {
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.print("Кол-во элементов: ");
        int count = scanner.nextInt();
        
        ArrayList<String> sequence = new ArrayList<String>();
        
        String temp;
        for(int i = 0; i < count; i++) {
           temp = scanner.next();
           if(!sequence.contains(temp))
               sequence.add(temp);
        }
        
        System.out.println(sequence);
    }

}
